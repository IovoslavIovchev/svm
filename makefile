CC=gcc
SRC=main.c vm.c regs.c
CFLGS = -Wall -Wextra -g -std=c11 -D DEBUG

example: sasm debug

debug:
	$(CC) $(CFLGS) $(SRC) -o svm

sasm:
	$(CC) $(CFLGS) sasm.c -o sasm
	sasm

clean:
	rm -f sasm *.sv svm

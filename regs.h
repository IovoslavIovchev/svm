#pragma once

#include <stdint.h>

typedef enum {
  S,
  P,
  R,
  F,
  E,
  SP,
  PC,
  REGS_SIZE
} reg_t;

typedef enum {
  Z,
  N,
  C
} flag_t;

#define STACK_SIZE (1 << 16)

typedef uint32_t reg_file_t[REGS_SIZE];

typedef uint32_t stack_t[STACK_SIZE];

#define pc regs[PC]
#define sp regs[SP]

void set_flag(reg_file_t, const flag_t, const uint8_t);
uint8_t get_flag(const reg_file_t, const flag_t);

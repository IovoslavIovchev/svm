# SVM

`SVM` is a **S**imple (or stupid, or suckless, it is open to interpretation..) (_stack based_, important to note!!!) **V**irtual **M**achine, written is C.

It does nothing of significant importance, and you most likely should not care about it.

_However_, if you are interested, you can check out the example
```sh
$ make           # compiles the assembler & the VM and assembles a few instructions
$ svm example.sv # executes the instructions in the VM, DUH...
```
## Registers
There are 4 general-purpose registers, 1 flag register, a `PC` register and a `SP` register. All of them are 32 bit.

## Flags
- `Z` - the zero flag
- `N` - the negative flag
- `C` - the carry flag

## Instructions
| Mnemonic | Operands | Description |
| - | - | - |
| HLT | - | Halts the program |
| INT | 1 | Handles an interrupt |
| PSH | [val] | Pushes `val` onto the stack |
| POP | - | Pops the top value off the stack |
| LD[X] | [val] | Loads `val` into the `[X]` register |
| CMP | - | Compares the top two values on the stack & sets the registers accordingly |
| JMP | [pc] | Sets the program counter to `pc` |
| JP[X] | [pc] | Sets the program counter to `pc` if the `[X]` register is set |
| JN[X] | [pc] | Sets the program counter to `pc` if the `[X]` register is not set |
| ADD | - | Pops the top two values off the stack, sums them, and pushes the result onto the stack |
| HCF | - | Halt and catch fire |

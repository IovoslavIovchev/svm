#include <stdio.h>
#include <stdint.h>

#include "instr.h"

int main(int argc, char** argv) {
  uint32_t arr[] = {
    JMP,
    3,
    HCF,
    PSH,
    3,
    PSH,
    352,
    CMP,
    JNZ,
    14,
    LDE,
    0,
    JMP,
    16,
    LDE,
    1,
    LDF,
    'h',
    INT,
    0x10,
    HLT
  };

  const char* name = argc > 1 ? argv[1] : "example.sv";

  FILE* hndl = fopen(name, "wb");

  fwrite(arr, sizeof(arr), 1, hndl);

  fclose(hndl);
}

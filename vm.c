#include "vm.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define in(c, block) case c: block; break;

// A simple macro for debugging and more verbose output
#ifdef DEBUG
#define dbg(...) printf(__VA_ARGS__)
#else
#define dbg(...)
#endif

#define LD_INSTR(R) \
  in(LD##R, { \
    dbg("LD" #R " %d\n", instrs[pc]); \
    regs[R] = instrs[pc++]; \
  })

#define JMP_INSTR(F) \
  in(JP##F, { \
    pc = get_flag(regs, F) ? instrs[pc] : pc + 1; \
    dbg("JP" #F " %d\n", pc); \
  }) \
  \
  in(JN##F, { \
    pc = get_flag(regs, F) ? pc + 1 : instrs[pc]; \
    dbg("JN" #F " %d\n", pc); \
  }) \

void interrupt(const instr_t instrs[], reg_file_t regs, stack_t stack) {
  switch (instrs[pc++]) {
  case 0x10:
    dbg("INT 0x10\n");
    printf("%c", regs[F]);
    dbg("\n");

    break;
  default:
    return;
  }
}

void exec(const instr_t instrs[], reg_file_t regs, stack_t stack) {
  for (;;) {
    dbg("> SP: %d, PC: %d\n", sp, pc);

    switch (instrs[pc++]) {
      in(PSH, {
        dbg("PSH %d\n",instrs[pc]);
        stack[sp++] = instrs[pc++];
      })

      in(POP, {
        dbg("POP");
        --sp;
      })

      in(INT, interrupt(instrs, regs, stack))

      LD_INSTR(S)
      LD_INSTR(P)
      LD_INSTR(R)
      LD_INSTR(F)
      LD_INSTR(E)

      in(ADD, {
        const uint32_t r = stack[--sp];
        const uint32_t l = stack[--sp];

        dbg("ADD %d %d\n", l, r);

        const uint32_t res = l + r;
        stack[sp++] = res;

        set_flag(regs, Z, res == 0);
        set_flag(regs, N, 0);
        set_flag(regs, C, res < r);
      })

      in(CMP, {
        dbg("CMP\n");
        const uint32_t x = stack[sp - 1];
        const uint32_t y = stack[sp - 2];

        const uint32_t z = x - y;
        const uint8_t carry = z > x;

        set_flag(regs, Z, z == 0);
        set_flag(regs, N, 0);
        set_flag(regs, C, carry);
      })

      in(JMP, {
        pc = instrs[pc];
        dbg("JMP %d\n", pc);
      })

      JMP_INSTR(Z)
      JMP_INSTR(N)
      JMP_INSTR(C)

      in(HCF, {
        dbg("HCF\n");
        for(;;);
      })

      in(HLT, {
        dbg("HLT\n");
        return;
      })
    }
  }
}

#pragma once

#include <stdint.h>

#include "regs.h"
#include "instr.h"

void exec(const instr_t*, reg_file_t, stack_t);

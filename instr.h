#pragma once

#include <stdint.h>

typedef enum {
    HLT,
    INT,
    PSH,
    POP,
    CMP,
    LDS,
    LDP,
    LDR,
    LDF,
    LDE,
    JMP,
    JPZ,
    JNZ,
    JPN,
    JNN,
    JPC,
    JNC,
    ADD,
    HCF,
} instr_t;

#include "vm.h"
#include "instr.h"
#include "regs.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: svm [file]\n");
    return 1;
  }

  FILE* fp = fopen(argv[1], "rb");

  // get the file size
  fseek(fp, 0, SEEK_END);
  uint64_t size = ftell(fp);
  rewind(fp);

  instr_t* instrs = (instr_t*) malloc(size);

  // read the instructions
  fread(instrs, size, 1, fp);

  reg_file_t regs;
  regs[PC] = 0;
  regs[SP] = 0;

  stack_t st;

  // execute the instructions
  exec(instrs, regs, st);

  fclose(fp);
  free(instrs);

  return regs[E];
}

#include "regs.h"

void set_flag(reg_file_t regs, const flag_t f, const uint8_t v) {
  const uint8_t b = 1 << f;

  if (v)
    regs[F] |= b;
  else
    regs[F] &= ~b;
}

uint8_t get_flag(const reg_file_t regs, const flag_t f) {
  return (regs[F] & (1 << f)) > 0;
}
